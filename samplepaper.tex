% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\input{packages.tex}

\hyphenation{re-so-lu-tion bio-lo-gic-al-ly}

\begin{document}
%
\title{Globally Optimized Super-Resolution of Diffusion MRI Data via Fiber Continuity}
\titlerunning{Fiber-Continuity Super-Resolution}

\authorrunning{Y.~Wu, et al.}
\author{Ye Wu \and Yoonmi Hong \and Sahar Ahmad \and Wei-Tang Chang \and Weili Lin \and Dinggang Shen \and Pew-Thian~Yap\textsuperscript{\Letter}}
% index{Wu, Ye}
% index{Hong, Yoonmi}
% index{Ahmad, Sahar}
% index{Chang, Wei-Tang}
% index{Lin, Weili}
% index{Shen, Dinggang}
% index{Yap, Pew-Thian}
\institute{
	Department of Radiology and Biomedical Research Imaging Center (BRIC), University of North Carolina, Chapel Hill, USA \\
	\email{ptyap@med.unc.edu}%\\
}
\blfootnote{This work was supported in part by NIH grants (NS093842 and EB006733)} 
\maketitle
%
\begin{abstract}
In this paper, we introduce a technique for super-resolution reconstruction of diffusion MRI, harnessing fiber-continuity (FC) as a constraint in a global whole-brain optimization framework.
% called global fiber-continuity (GFC)  reconstruction, to recover the fine structural details of diffusion MRI using global fiber orientation continuity information. 
% FCSR aims to reduce the acquisition time while improving image quality.
FC is a biologically-motivated constraint that relates orientation information between neighboring voxels. We show that it can be used to effectively constrain the inverse problem of recovering high-resolution data from low-resolution data. Since voxels are inter-related by FC, we devise a global optimization framework that allows solutions pertaining to all voxels to be solved simultaneously.
%and the global optimization allows information from 
% This allows tracing of small fiber bundles connecting very small-scaled cortical or sub-cortical regions. 
% The proposed method is based on dual concepts of intra-voxel architecture estimation and super-resolution to simultaneously model the diffusion signal in multiple shells. 
% In particular, the fiber orientation continuity regularization is incorporated to account for signal reconstruction in non-local voxels.
We demonstrate that the proposed super-resolution framework is effective for diffusion MRI data of a glioma patient, a healthy subject, and a macaque.

%The comparison with classical interpolation methods over multiple datasets (including scans from glioma patients, highly slice-undersampled healthy subjects, and macaque), demonstrates the improvements of FCSR in reconstructing high-quality images. Moreover, the tractography analysis shows the potential of FCSR to accurately analyze the white matter of brain architecture.

\keywords{Diffusion MRI \and Super-resolution \and Fiber continuity}
\end{abstract}
%
\section{Introduction}
Diffusion magnetic resonance imaging (DMRI) with high angular and spatial resolution is plagued by low signal-to-noise ratio (SNR) and long acquisition time~\cite{johansen-berg_diffusion_2013}. These are inevitable consequences of the smaller voxel size and the large number of diffusion-weighted (DW) volumes that need to be acquired for multiple diffusion directions and scales. 
Instead of relying on expensive scanner upgrades and sophisticated MRI sequences, super-resolution (SR) techniques can be used to infer high-resolution (HR) images from low-resolution (LR) images. 

Regularization-based SR methods harness constraints such as smoothness, sparsity, and low-rank \cite{fuster_super_resolution_2016,haldar_improved_2013,baete_low_2018,shi_lrtv_2015,ning_joint_2016,lum2015super} to infer HR information. 
These methods leverage inter-voxel spatial redundancy that is typical in images for data recovery. For example, in \cite{goh2009estimating}, spatial regularization is used to improve estimation of orientation distribution functions.
%introduced which improves the performance by assuming smoothness: neighboring voxels are assumed to be similar. 
Ning et al. \cite{ning_joint_2016} coupled compressed sensing with SR for accelerated HR DMRI reconstruction and demonstrated that spatial regularization reduces sensitivity to noise. 
%In total, these methods impose directly smoothness regularization on the signal domains, which may limit the representation of DMRI signal on the multi-shell scheme.

Another form of regularization that is more specific to DMRI is fiber continuity (FC). FC is a biologically-motivated constraint
that arises from the spatially-continuous nature of fiber tracts. The orientation information at each voxel location captures localized information about white matter pathways. Based on this observation, the FC constraint requires that orientations transit smoothly across voxels and is therefore a means of linking information within and across voxels.   
Recent studies have shown that FC  \cite{calamante2010track,reisert_about_2012,yap2014fiber,wu_mitigating_2019} can be harnessed for enhancing image resolution and improving the estimation of fiber orientation distributions. %\todo{Some sentences in third and fourth paragraphs are repeated.}

In this paper, we propose a technique for SR reconstruction of DMRI data by harnessing fiber continuity (FC) constraint-based on intra-voxel orientation information. Note that our method does not rely explicitly on tractography. The term “fiber continuity” (FC) comes from the fact that at each voxel diffusion MRI captures the local orientation of a fiber bundle. Since fibers are continuous spatially, the orientations are expected to vary smoothly across space. 
%FC is a biologically motivated constraint where, owing to the spatially continuous nature of fiber tracts, intra-voxel orientations should be relatively consistent across voxels. 
We show that FC in tandem with signal representation based on asymmetric fiber orientation distributions \cite{wu_mitigating_2019,bastiani_improved_2017} can be used to regularize the ill-posed inverse problem associated with the recovery of the HR DMRI data from its LR counterpart. We 
\begin{inparaenum}[(i)]
  \item formulate a novel SR technique that uses intra-voxel orientation information; 
  \item use a global optimization framework for the spatio-angular recovery of multi-shell DMRI data, reducing sensitivity to noise; 
  \item demonstrate the efficacy of the technique in tumorous and healthy human brains and a macaque brain; and 
  \item demonstrate the utility of the technique in recovering HR data from highly incomplete or under-sampled LR DMRI data \cite{hong2020side}. 
\end{inparaenum}
Experimental results indicate that our technique enables the recovery of rich structural details.

\section{Methods}
\subsection{Forward Model}


The LR DW volumes are considered as down-sampled versions of HR DW volumes that need to be estimated. For this purpose, we let 
%$\mathbf{Y} \subset \mathbb{R}_{N_{xyz}}^3 \times \mathbb{S}_{N_{g}}^2$ 
$\mathbf{Y} \in \mathbb{R}^{N_{v} \times M_g}$ 
%\todo{need to confirm}
denote a set of LR DW volumes with $N_{v}$ voxels per volume and $M_{g}$ gradient directions across different shells.
The LR volumes $\mathbf{Y}$ are related to the HR volumes $\mathbf{S} \in \mathbb{R}^{M_{v} \times M_{g}}$ by
\begin{equation}
\mathbf{Y} = \mathbf{DS} + \boldsymbol{\mu},
\end{equation}
where $\mathbf{D}$ is a down-sampling matrix that averages neighboring voxels 
and $\boldsymbol{\mu}$ denotes noise. 
%$\mathbf{B} \odot \mathbf{Y}$ is observed data padded with zeros.
%
The sampling of $\mathbf{Y}$ can be encoded by a binary matrix $\mathbf{B}$ such that 
\begin{equation}\label{eq:sr}
     \mathbf{B} \odot \mathbf{Y} = \mathbf{B} \odot (\mathbf{DS} + \boldsymbol{\mu}),
\end{equation}
where $\odot$ is the element-wise multiplication operator.
%\begin{equation}\label{eq:sr}
%\mathbf{Y} = \mathbf{B} \mathbf{DSQ}^{\top} + \boldsymbol{\mu},
%\end{equation}
The model is general and can account for different spatio-angular subsampling schemes determined by $\mathbf{B}$ and $\mathbf{D}$. 

\subsection{Fiber Continuity (FC)}

Our method uses fiber continuity across voxels to regularize the inverse problem of recovering the HR volumes. 
Fiber continuity \cite{wu_mitigating_2019} is a natural constraint supporting the fact that orientations should be consistent along fiber tracts.
%
A model based on asymmetric fiber orientation distribution functions (AFODFs) \cite{wu_mitigating_2019} is used to represent the sub-voxel configurations.
%<<<<<<< Updated upstream
%In DMRI, the signal profile is typically assumed to be antipodal symmetric, implying that orientation in a positive hemisphere is always identical to its counterpart in the negative hemisphere. Symmetric FODFs will always result in fiber dispersion in cases where fibers converge in one direction and fan out in the opposite direction, leading to fanning out in both directions. To achieve sub-voxel fiber continuity, we construct AFODF by incorporating the information from neighboring voxels, allowing FODF asymmetry to better represent the complex configurations \cite{karayumak_asymmetric_2018,bastiani_improved_2017,reisert_about_2012}.  
%The discontinuity of AFODFs, $\boldsymbol{\mathcal{F}}_{\text{aniso}}$, over all voxels and direction $\mathbf{u}$ is measured as
%=======
%
%<<<<<<< HEAD
In DMRI, the signal profile is typically assumed to be antipodal symmetric, implying that orientation in a positive hemisphere is always identical to its counterpart in the negative hemisphere. 
Symmetric FODFs are not sufficient in regions with, for example, fanning and bending fiber trajectories.
%will always result in fiber dispersion in cases where fibers converge in one direction and fan out in the opposite direction, leading to fanning out in both directions. 
To improve sub-voxel fiber continuity, we estimate AFODFs by incorporating information from neighboring voxels, allowing FODF asymmetry to better represent complex configurations \cite{karayumak_asymmetric_2018,bastiani_improved_2017,reisert_about_2012}.  
%=======
%In DMRI, the signal profile is typically assumed to be antipodal symmetric, implying that orientation in a positive hemisphere is always identical to its counterpart in the negative hemisphere. To achieve sub-voxel FC, the AFODFs incorporate information from neighboring voxels to better represent the complex configurations \cite{wu_mitigating_2019,karayumak_asymmetric_2018,bastiani_improved_2017,reisert_about_2012}.  
%>>>>>>> dc21c4a98464e28bff61a357522e189d13dafa21
%by propagating a spatially structured DMRI singal prior, where the structure originates from the spatial coherence of the asymmetric fiber orientation between neighboring voxels. 
The discontinuity of AFODFs, $\boldsymbol{\mathcal{F}}_{\text{aniso}}$, over all voxels and directions ($\mathbf{u}\in\mathbb{S}^2$) is measured as
%>>>>>>> Stashed changes
\begin{equation}
 \Phi(\boldsymbol{\mathcal{F}}_{\text{aniso}})=
\int_{\mathbf{u}\in\mathbb{S}^2}\left\| \boldsymbol{\mathcal{F}}_{\text{aniso}}(\mathbf{u}) \otimes \mathbf{W}(\mathbf{u}) - \boldsymbol{\mathcal{F}}_{\text{aniso}}(\mathbf{u}) \right\|_{\text{F}}^2 d\mathbf{u},
\end{equation}%\todo{Please include $\mathbf{p}$ and $\mathbf{u}$ in the equation. $\|\|$ is Frobenius norm?}
where $\mathbf{W}$ is a normalized directional probability distribution function as defined in \cite{wu_mitigating_2019} and $\otimes$ is a column-wise multiplication operator.

\subsection{Spherical Convolution}

We use a multi-tissue spherical convolution model to represent the diffusion signal at each voxel of $\mathbf{S}$. Specifically, $\mathbf{S}$ can be decomposed as $\mathbf{S}=\boldsymbol{\mathcal{RX}}$, with $\boldsymbol{\mathcal{R}}=\left[\mathbf{R}_{\text{aniso}},
\mathbf{R}_{\text{aniso}},
\mathbf{R}_{\text{iso},1},\cdots,{\mathbf{R}_{\text{iso},n}} \right]$ being the multi-tissue axially symmetric response function (RF) consisting of an anisotropic component $\mathbf{R}_{\text{aniso}}$ and $n$ isotropic compartments $\{\mathbf{R}_{\text{iso},i}\}_{i=1}^n$ as defined in \cite{wuAsymmetrySpectrumImaging2019}. Each column of $$\boldsymbol{\mathcal{X}} = \left[ \mathbf{X}_{\text{aniso}}^{+}, \mathbf{X}_{\text{aniso}}^{-}, \mathbf{X}_{\text{iso},1},\cdots,\mathbf{X}_{\text{iso},n} \right]^{\top}$$ is the spherical harmonics (SH) coefficients of AFODF at each voxel location. 
The AFODF is represented by two sets of even-order spherical harmonics~\cite{wu_mitigating_2019}, capturing potentially asymmetric sub-voxel fiber configurations.
%To robustly represent the fiber continue with AFODF, \cite{wu_mitigating_2019} employs two sets of even-order spherical harmonics coefficients to capture the asymmetry and being demonstrated in recovery effective subvoxel fiber configure. 
That is,
\begin{equation}\label{eq:afodf_sh}
\boldsymbol{\mathcal{F}}_{\text{aniso}}(\mathbf{u})=
\begin{cases}
\mathbf{A}(\mathbf{u})\cdot\boldsymbol{\mathbf{X}}_{\text{aniso}}^{+}, & \mathbf{u}\in\mathbb{S}_{+}^2,\\
\mathbf{A}(\mathbf{u})\cdot\boldsymbol{\mathbf{X}}_{\text{aniso}}^{-}, & \mathbf{u}\in\mathbb{S}_{-}^2,\\
\end{cases}
\end{equation}
where $\mathbf{A}(\mathbf{u})$ is the real even-order SH basis function sampled in direction $\mathbf{u}$, and the column vectors of $\boldsymbol{\mathbf{X}}_{\text{aniso}}^{+}$ and $\boldsymbol{\mathbf{X}}_{\text{aniso}}^{-}$ are the corresponding SH coefficients for the positive and negative hemispheres ($\mathbb{S}_{+}^2$ and $\mathbb{S}_{-}^2$). 

\subsection{Super-Resolution Reconstruction}
Based on Eq.~\eqref{eq:sr}, 
we estimate $\mathbf{S}$ and $\boldsymbol{\mathcal{X}}$ by solving
\begin{equation}
    \min_{\mathbf{S},\boldsymbol{\mathcal{X}}} \frac{1}{2}\left\| \mathbf{B} \odot ( \mathbf{Y} - 
%    \mathbf{B} \odot 
    \mathbf{DS} ) \right \|^2 + \lambda \left\| \boldsymbol{\mathcal{L}}\boldsymbol{\mathcal{X}} \right\|^2, \\
    ~\text{s.t.}~ \\
    \left\{
        \begin{array}{lr}
            \mathbf{S} = \boldsymbol{\mathcal{R}}\boldsymbol{\mathcal{X}},\\
            \boldsymbol{\mathcal{A}}\boldsymbol{\mathcal{X}} \succeq 0,\\
            \Phi(\boldsymbol{\mathcal{F}_{\text{aniso}}}) \le \epsilon,\\
            \mathbf{S} \succeq 0,
        \end{array}
    \right. \label{eq:model2}
\end{equation}
%where the FODF is obtained by modeling the signal with high order SH series \cite{tournier_robust_2007,auria_structured_2015} using a Laplace-Beltrami regularization defined by $\mathbf{L}$ with entries $\ell_j(\ell_j+1)$ along the diagonal ($\ell_j$ is the order associated with the $j$-th coefficient).
where the Laplace-Beltrami regularization \cite{tournier_robust_2007} is realized via $\boldsymbol{\mathcal{L}}=\text{diag}\left[\mathbf{L},\mathbf{L},0,\cdots,0\right]$ where $\mathbf{L}_{jj}= \ell_j(\ell_j+1)$ with $\ell_j$ being the order associated with the $j$-th coefficient.
%The Laplace-Beltrami regularization is a natural measure of smoothness for functions defined on the unit sphere and has been used in many image processing applications \cite{descoteaux_regularized_2007}. 
$\lambda$ is the regularization parameter and the matrix $\boldsymbol{\mathcal{A}}= \text{diag}\left[\mathbf{A},\mathbf{A},a_0,\cdots,a_0\right]$ maps the SH coefficients $\boldsymbol{\mathcal{X}}$ to the AFODF amplitudes for imposing AFODF non-negativity. $a_0=\sqrt{\frac{1}{4\pi}}$ is the $0$-th order SH basis function. 

%Eq.~\eqref{eq:model2} is a generalized SR framework valid for (i) the recently proposed slice-interleaved encoding; (ii) joint \textit{xq}-space  SR when $\mathbf{B}$ is a matrix of ones; (iii) \textit{x}-space SR when $\mathbf{B}$ is a matrix of ones with the same length of $\mathbf{S}$ and $\mathbf{Y}$; (iv)  \textit{q}-space SR when $\mathbf{B}$ is a matrix of ones and $\mathbf{Q}$ is an identity matrix.
%To reduce sensitivity to noise, we simultaneously recover both fiber orientation distribution across voxels and multi-shell DMRI signals. 
Problem~\eqref{eq:model2} is solved using alternating direction method of multipliers (ADMM) \cite{boyd_distributed_2011}. Specifically, the 
%constrained multi-task linear least-squares 
problem can be cast as a strictly convex quadratic programming (QP) problem, which can be solved by an operator splitting solver for quadratic programs (OSQP) \cite{stellato_osqp_2018}. Following common super-resolution frameworks \cite{shi_lrtv_2015,ning_joint_2016,lum2015super}, we used linearly interpolated data to initialize ADMM.

\subsection{Implementation Details}
A white matter (WM) RF is estimated 
%from the diffusion signal for 
for
each b-shell
from
%which is estimated by averaging the
the average of the  
reoriented diffusion signal profiles of voxels with high anisotropy in WM.
% To characterize DMRI signal with the effects of changes in microstructural properties across population, we described the afore-mentioned metric to account for the whole diffusion spectrum in isotropic component.
%To fully representation for the DMRI signal with the effects of changes in microstructural properties across the population,  
Also isotropic RFs 
%are estimated 
are set
with diffusivity values ranging from $0.1 \times 10^{-3}\,\text{mm}^2/\text{s}$ to $3 \times 10^{-3}\,\text{mm}^2/\text{s}$ and step size $0.2 \times 10^{-3}\,\text{mm}^2/\text{s}$. 
This will allow better characterization of tissue microstructure in healthy and tumorous brains.
%The whole spectrum of diffusion characteristics will allow the DMRI signal to be estimated accurately despite changes in microstructural properties across different studies, e.g. tumor patient study.
%\todo{I don't think we `estimate' the isotropic RFs.}

We used k-fold cross-validation to determine the optimal regularization parameter $\lambda$ in~\eqref{eq:model2}. Two thousand random white matter voxels from an HCP DMRI dataset and 61 candidate values for $\lambda$, logarithmically spanning the interval between 0.01 and 10, were considered \cite{wu_mitigating_2019,bastiani_improved_2017}. We found that the optimal value was 0.01. This value was applied to all experiments. 

\section{Experimental Results}
We compared our method, fiber-continuity super-resolution (FCSR), with four existing methods, including bicubic interpolation, Lanczos-3 resampling, TV-L1 reconstruction \cite{mani2015acceleration}, and TV-L2 reconstruction \cite{marquina2008image}. We assumed that LR DW volumes $\mathbf Y$ are spatially downsampled from HR DW volumes $\mathbf S$ by a factor of 2 for each dimension. 
%
Objective evaluation was performed using Blind/Referenced Image Spatial Quality Evaluator (BRISQUE) \cite{mittal2012no} and Naturalness Image Quality Evaluator (NIQE) \cite{mittal2012making}. Both BRISQUE and NIQE are no-reference image quality scores, with lower values indicating better image quality. 
%\subsection{Datasets}
Three datasets, summarized in Table~\ref{tab:my_label}, were used for validation, involving 
\begin{inparaenum}[(i)]
  \item a pre-surgical glioma patient \cite{aerts_modeling_2018}; 
  \item a healthy subject with data acquired using slice-interleaved diffusion encoding (SIDE) \cite{hong2020side}; and %\todo{Cite Yoonmi's ISMRM abstract?}
  \item a macaque with histological data \cite{schilling_confirmation_2018}; 
\end{inparaenum}
%>>>>>>> Stashed changes

\begin{table}[t]
	\centering
	\renewcommand{\arraystretch}{1.2}
	\setlength{\tabcolsep}{3pt}
	\caption{Datasets}
\begin{tabular}{ccccc}
	\hline 
	\multirow{2}{*}{} & \multirow{2}{*}{Glioma \cite{aerts_modeling_2018} } & \multirow{2}{*}{SIDE \cite{hong2020side}} & \multicolumn{2}{c}{Macaque \cite{schilling_confirmation_2018} } \tabularnewline
	\cline{4-5} \cline{5-5} 
	&  &  & LR &HR \tabularnewline
	\hline 
	Dimensions & $96 \times 96 \times 60$ & $128 \times 128 \times 100$& $48 \times 96 \times 64$ & $96 \times 192 \times 128$ \tabularnewline
	Resolution & $2.5\,\text{mm}^3$ & $1.5\,\text{mm}^3$ & $0.8\,\text{mm}^3$ & $0.4\,\text{mm}^3$ \tabularnewline
	Acceleration &  N/A & Multiband$\,=5$ &  \multicolumn{2}{c}{N/A}    \tabularnewline
	b-value (s/mm$^2$) &  700,1200,2800 &  500,1000,2000,3000&  \multicolumn{2}{c}{3000,6000,9000,120000}  \tabularnewline
	\# Gradient directions &  100 &  160 &  \multicolumn{2}{c}{404}  \tabularnewline
	\# Non-DW volumes & 2 & 1 & \multicolumn{2}{c}{16}  \tabularnewline
	\hline 
  \end{tabular}
\label{tab:my_label}
\end{table}

\subsection{Glioma Dataset}

Representative results for fractional anisotropy (FA), isotropic volume fraction (IVF),  apparent diffusion coefficient (ADC), %\todo{Use IVF in figures.}
 intra‐cellular volume fraction (ICVF)\footnote{Computed using AMICO \cite{daducci2015accelerated}.}, and tissue maps are shown in Fig.~\ref{fig:cancer}. 
Among the methods, FCSR recovers more structural details, especially in the zoomed-in regions. 
Moreover, FCSR retains the shape of the tumor, similar to the T1-weighted image that is shown in the insets. %\todo{Where is the T1w image? Q: In the IVF map, the T1 image is included in the rectangle box at the bottom-right of each subfigure} 
FCSR results in lower BRISQUE and NIQE scores, indicating better image quality. 
%These results confirm that the improvement given by the proposed method is striking compared to the other methods. 
Figure~\ref{fig:fiber} shows that FCSR improves the tractography of fiber bundles connecting cortical or sub-cortical regions. 


\begin{figure*}[!tb]\centering
	\includegraphics[width=\textwidth]{fig/Cancer.png}
	\caption{Color FA, IVF, ADC, ICVF, and WM tissue maps of a patient with glioma. NIQE and BRISQUE scores are shown on the right. The T1-weighted reference image is shown in the insets of the IVF results.}
	\label{fig:cancer}
\end{figure*}%\todo{The label `Index' should be removed. Same for other figures.}

\begin{figure*}[!tb]\centering
	\includegraphics[width=\textwidth]{fig/fiber.png}
	\caption{Fiber bundles given by (a) the original LR data, (b) bicubic interpolation, (c) Lanczos-3 resampling, (d) TV-L1 reconstruction, (e) TV-L2 reconstruction, and (f) FCSR.}
	\label{fig:fiber}
\end{figure*}


\subsection{SIDE Dataset}
We also evaluated FCSR with SIDE \cite{hong2020side} data acquired with acceleration factors $R = 2, 4$, and $10$. Collecting the full data ($R=1$) took a total acquisition time of 8.32 minutes. Therefore  the acquisition time is $8.32/R$ minutes for acceleration factor $R$. 
The results are summarized in Fig.~\ref{fig:side}, demonstrating that FCSR recovers more structural details especially in the cortical regions. The effectiveness of FCSR is further confirmed by the lower NIQE and BRISQUE scores. Note that $R = 10$ corresponds to less than one minute of acquisition time. Coupling SIDE with FCSR allows rich information on tissue microstructure to be collected in a very short amount of time.

We also evaluated FCSR by ICVF values along tractography streamlines. Figure~\ref{fig:alongtract}(d,e) show that FCSR improves the tractography of u-fibers connecting small cortical or subcortical regions, with high consistency between full acquisition (i.e., FCSR (HR)) and acquisitions with different acceleration factors. 

%To further evaluate the advantage of FCSR in recovering structure, 
Figure~\ref{fig:shfod} compares the FODFs generated by (a) spatially upsampled SH coefficients \cite{tournier_robust_2007} of the full LR data ($R=1$) and (b) FCSR. It can be seen that details near the cortex cannot be sufficiently recovered by upsampling via interpolation. 
On the other hand, FCSR yields superior orientation details in the cortical region, critical 
%powerful performance to recover sub-voxel diffusion signals and overcome the effect from the partial volume effect on reconstruction. 
for mitigating gyral bias and improving the estimation of cortico-cortical connections \cite{wu_mitigating_2019}.

\begin{figure*}[!tb]\centering
	\includegraphics[width=\textwidth]{fig/SIDE.png}
	\caption{Color FA map, WM tissue map, GM tissue map, ICVF map, and FODF glyphs of the SIDE data acquired from a healthy subject. NIQE and BRISQUE scores are shown on the right.}
	\label{fig:side}
\end{figure*}

\begin{figure*}[!tb]\centering
	\includegraphics[width=\textwidth]{fig/alongtract.png}
	\caption{The tract streamlines and along-tract ICVF values for five major white matter bundles. 
%    ICVF values are plotted versus position from tract origin. 
    On the right, the ICVF values averaged across streamlines in a bundle are plotted along-tract locations. 
    The Pearson correlation coefficients ($r$) and the \textit{p}-values of mean ICVF values between the full and accelerated acquisitions are shown.}
	\label{fig:alongtract}
\end{figure*}

\begin{figure*}[!tb]\centering
	\includegraphics[width=\textwidth]{fig/shfod.png}
	\caption{Comparison of HR FODFs computed by (a) upsampling the SH coefficients and (b) FCSR of the full LR data ($R=1$). Close-up views are shown for the cortical region marked by the red rectangle.}
	\label{fig:shfod}
\end{figure*}

\subsection{Macaque Dataset}
%Next, we evaluated our method with the Macaque subject. 
%The quantitative results for the different methods are summarized in
Figure~\ref{fig:monkey} summarizes the results for the macaque dataset, including color FA, axon diameter index, and tissue maps. Note that HR and SR DMRI data are reconstructed from original LR and HR DMRI data, respectively.  
%We assess the closeness of the reconstructed HR DMRI to the original HR DMRI via the microstructure index. 
The results again indicate that FCSR is superior in preserving details and consistent across HR and SR reconstruction.  The efficacy of FCSR is confirmed by the mean squared error (MSE) and structural similarity index (SSIM), which are computed between the reconstructed HR data and the original HR data,  averaged across volumes.


\begin{figure*}[!tb]\centering
	\includegraphics[width=\textwidth]{fig/Monkey.png}
	\caption{Color FA, axon diameter, and GM tissue maps of the macaque brain. MSE and SSIM values are shown on the right. FCSR (HR) and FCSR (SR) are reconstructed from original LR and HR DMRI, respectively.}
	\label{fig:monkey}
\end{figure*}

\section{Conclusion}
We demonstrated that spatial resolution can be improved using the fiber-continuity constraint in a global whole-brain optimization framework.
Our evaluation with datasets acquired with different resolutions, imaging sequences, and scanners for humans and non-human primates indicate that our method is effective in resolution enhancement.

%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
\bibliographystyle{llncs_splncs}
\bibliography{references}
\end{document}
